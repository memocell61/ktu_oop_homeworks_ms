#include <iostream>
#include <string>

using namespace std;



int isVowel(char inChr){
   string vowels = "aeiouyAEIOUY";

   if (vowels.find(inChr) != string::npos)
    return 1;
   else
    return 0;
}

string rotate(string inStr) {
  string retString;
  char a = inStr[0];

  retString = inStr.substr(1, inStr.size()-1);
  retString += a;
  return retString;
}

int main()
{
    string s;
    cout << "Please enter a string" << endl;
    cin >> s;
    int strLen = s.size();

    if (!isVowel(s[0])) s += "-";

    if (isVowel(s[0])) {
      s += "way";
    } else {
      while (!isVowel(s[0])) {
        s = rotate(s);
      }
      s += "ay";
    }
    strLen --;
    cout << s;

    return 0;
}
